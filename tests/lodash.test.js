const _ = require('lodash');
const chai = require("chai");
const expect = chai.expect; 


test('Should reverse an array', () => {
    const array = [1, 2, 3];
    let reversedArr = _.reverse(array);
    expect(reversedArr).to.eql([3, 2, 1]);
})