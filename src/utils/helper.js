
let sorting = (array) => {
    
	let sortedArray = array.sort((a, b) => {
        if(a > b) 
			return 1;
        else      
			return -1;
	  });
	
	return sortedArray;
	
}

let compare = (a, b) => {
    if(parseInt(a['PM2.5']) > parseInt(b['PM2.5'])) 
		return 1;
    else 
		return -1;
}

let average = (nums) => {
	 	
	let sum = 0;
	for(let i = 0; i < nums.length; i++) {
		sum = sum + nums[i];
	}
	   
	   let ave = sum / nums.length;
	   
	   return Math.round(ave*100)/100;
}


module.exports = {
    sorting,
    compare,
    average
}